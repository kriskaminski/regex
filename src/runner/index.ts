/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import {
    ConditionBlock,
    condition,
    tripetto,
} from "tripetto-runner-foundation";

export interface IRegExCondition {
    variable?: string;
    regex: string;
    invert: boolean;
}

@tripetto({
    type: "condition",
    identifier: PACKAGE_NAME,
})
export class RegExCondition extends ConditionBlock<IRegExCondition> {
    @condition
    validate(): boolean {
        if (this.props.variable) {
            const variable = this.variableFor(this.props.variable);

            if (variable) {
                try {
                    const regex = this.props.regex || "";
                    const literalSignLeft = regex.indexOf("/");
                    const literalSignRight = regex.lastIndexOf("/");

                    return (
                        literalSignLeft === 0 &&
                        literalSignRight > literalSignLeft &&
                        new Function(
                            "value",
                            `return ${
                                this.props.invert ? "!" : ""
                            }${regex}.test(value)`
                        )(variable.string)
                    );
                } catch {
                    return false;
                }
            }
        }

        return false;
    }
}
