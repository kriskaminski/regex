/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    Forms,
    Slots,
    affects,
    definition,
    each,
    editor,
    pgettext,
    populateSlots,
    tripetto,
} from "tripetto";
import { IRegExCondition } from "../runner";

/** Assets */
import ICON from "../../assets/icon.svg";

@tripetto({
    type: "condition",
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    context: "*",
    icon: ICON,
    get label() {
        return pgettext("block:regex", "Regular expression");
    },
})
export class RegExCondition extends ConditionBlock implements IRegExCondition {
    @definition
    @affects("#name")
    variable?: string;

    @definition
    regex = "";

    @definition
    invert = false;

    // Return an empty label, since the variable name is in the block name already.
    get label() {
        return "";
    }

    get name() {
        return (
            (this.variable && `${this.type.label} @${this.variable}`) ||
            this.type.label
        );
    }

    @editor
    defineEditor(): void {
        const slotsList: Forms.IDropdownOption<string>[] = [];
        const collectionsList: Forms.IDropdownOption<string>[] = [];
        const slots: {
            [variable: string]: Slots.Slot;
        } = {};

        each(
            this.map &&
                populateSlots(this, {
                    mode: "validated",
                }),
            (slot) => {
                if (slot.slots) {
                    collectionsList.push({
                        optGroup: slot.label,
                    });

                    each(slot.slots, (subSlot) => {
                        if (subSlot.id && subSlot.slot) {
                            collectionsList.push({
                                label: subSlot.label,
                                value: subSlot.id,
                            });

                            slots[subSlot.id] = subSlot.slot;
                        }
                    });
                } else if (slot.id && slot.slot) {
                    slotsList.push({
                        label: slot.label,
                        value: slot.id,
                    });

                    slots[slot.id] = slot.slot;
                }
            }
        );

        this.editor.form({
            title: pgettext("block:regex", "Input"),
            controls: [
                new Forms.Dropdown(
                    [...slotsList, ...collectionsList],
                    this.variable
                )
                    .placeholder(
                        pgettext(
                            "block:regex",
                            "Select the input variable to use..."
                        )
                    )
                    .on((variable) => {
                        this.variable = variable.value || undefined;
                        this.slot =
                            (variable.value && slots[variable.value]) ||
                            undefined;
                    })
                    .autoFocus(),
            ],
        });

        this.editor.form({
            title: pgettext("block:regex", "Regular expression"),
            controls: [
                new Forms.Text("singleline", Forms.Text.bind(this, "regex", ""))
                    .placeholder(
                        pgettext(
                            "block:regex",
                            "Regex literal (for example /ab+c/)"
                        )
                    )
                    .autoValidate((regex) => {
                        if (!regex.value) {
                            return "unknown";
                        }

                        try {
                            const literalSignLeft = regex.value.indexOf("/");
                            const literalSignRight = regex.value.lastIndexOf(
                                "/"
                            );

                            return literalSignLeft === 0 &&
                                literalSignRight > literalSignLeft &&
                                new Function("", `return ${regex.value}`)()
                                ? "pass"
                                : "fail";
                        } catch {
                            return "fail";
                        }
                    }),
                new Forms.Checkbox(
                    pgettext("block:regex", "Invert regular expression"),
                    Forms.Checkbox.bind(this, "invert", false)
                ),
            ],
        });
    }
}
